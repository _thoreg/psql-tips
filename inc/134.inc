    <div class="tip">The <code>\gset prefix</code>
      metacommand will execute the result of the current query or the last query
      if the current query buffer is empty and assign variables named as the
      column names with the given prefix and the value. It only works if the query returns only one
      row.
      <pre><code class="hljs bash">laetitia=# laetitia=# select *
from test
where id=5;
 id | value 
----+-------
  5 | bla
(1 row)

laetitia=# \gset test_
laetitia=# \echo 'id: ' :test_id ', value: ' :test_value
id:  5 , value:  bla</code></pre>This feature is available since Postgres 9.3.
	</div>
